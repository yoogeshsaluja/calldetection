package com.om.calldetection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.widget.Toast;


public class PhoneStateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            System.out.println("Receiver start");
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);


            if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))){
                showToast(context,"  Call Started"+incomingNumber);

             }else  if (state.equals(TelephonyManager.EXTRA_STATE)){
                showToast(context,"Call End"+incomingNumber);
             }
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    private void showToast(Context context,String msg){
        Toast toast= Toast.makeText(context, msg, Toast.LENGTH_LONG);

        toast.setGravity(Gravity.CENTER,0,0);
        toast.show();

    }
}


