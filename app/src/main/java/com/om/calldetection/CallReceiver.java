package com.om.calldetection;

import android.content.Context;
import android.widget.Toast;

import java.util.Date;

public class CallReceiver extends PhonecallReceiver {

    @Override
    protected void onIncomingCallStarted(Context ctx, String number, Date start) {
        Toast.makeText(ctx, "onIncomingCallStarted: "+number+" start at"+start, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onOutgoingCallStarted(Context ctx, String number, Date start) {
        Toast.makeText(ctx, "onOutgoingCallStarted: "+number+" start at"+start, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onIncomingCallEnded(Context ctx, String number, Date start, Date end) {
        Toast.makeText(ctx, "onIncomingCallEnded: "+number+" end at"+start, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onOutgoingCallEnded(Context ctx, String number, Date start, Date end) {
        Toast.makeText(ctx, "onOutgoingCallEnded: "+number+" start at"+start, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onMissedCall(Context ctx, String number, Date start) {
        Toast.makeText(ctx, "onMissedCall: "+number+" start at"+start, Toast.LENGTH_SHORT).show();

    }

}